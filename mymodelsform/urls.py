# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('mymodelsform.views',
                       # Examples:
                       # url(r'^$', 'demofield.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^$', 'article_name', name='article_name'),
                       url(r'^redirect/$', 'article_redirect', name='article_redirect'),
                       )
