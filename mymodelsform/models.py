# -*- coding: utf-8 -*-
from django.db import models


class Article(models.Model):
    class Meta():
        db_table = 'mymodelsform_app'

    title = models.CharField(max_length=100, blank=False, verbose_name='название')
    text = models.TextField(max_length=100, verbose_name='текст')

    def __unicode__(self):
        return self.title



