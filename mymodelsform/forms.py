# -*- coding: utf-8 -*-
from django.forms import ModelForm
from mymodelsform.models import Article


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'text']