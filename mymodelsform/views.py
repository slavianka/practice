# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponseRedirect
from mymodelsform.models import Article
from mymodelsform.forms import ArticleForm


def article_name(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            print form.cleaned_data
            form.save()
        # return HttpResponseRedirect('/model/redirect/')
        return HttpResponseRedirect('/homework/model/')

    else:
        form = ArticleForm()

    vivod = Article.objects.last()

    return render(request, 'article.html', {'form': form, 'vivod': vivod})


def article_redirect(request):
    return render_to_response('article.html')
