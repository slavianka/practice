# -*- coding: utf-8 -*-
"""practice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf.urls.static import static  # относится к ckeditor
from django.conf import settings  # относится к ckeditor

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('school.urls', namespace='school')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^auth/', include('loginsys.urls', namespace='auth')),
    url(r'^homework/model/', include('mymodelsform.urls', namespace='mymodelsform')),
]


urlpatterns += patterns('',
                        url(r'^ckeditor/', include('ckeditor.urls')), # относится к ckeditor
                        ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # относится к ckeditor

