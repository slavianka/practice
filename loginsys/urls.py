# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns


# urlpatterns = [
#
# ]

urlpatterns = patterns('loginsys.views',
                        # Examples:
                        # url(r'^$', 'demofield.views.home', name='home'),
                        # url(r'^blog/', include('blog.urls')),
                        url(r'^login/$', 'login', name='login'),
                        url(r'^logout/$', 'logout', name='logout'),
                        url(r'^register/$', 'register', name='register'),
                        )




# urlpatterns = patterns('',
#                        # Examples:
#                        # url(r'^$', 'firstapp.views.home', name='home'),
#                        # url(r'^blog/', include('blog.urls')),
#
#                        url(r'^login/$', 'loginsys.views.login'),
#                        url(r'^logout/$', 'loginsys.views.logout'),
#                        url(r'^register/$', 'loginsys.views.register'),
#
#                        )
