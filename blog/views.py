from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.shortcuts import render_to_response, redirect
from blog.models import Blog, Comments
from forms import CommentForm
from django.core.context_processors import csrf
from django.core.paginator import Paginator
from django.contrib import auth


# def post_all(request):
#     i = Blog.objects.all()
#     return render_to_response('post_all.html', {'key_post_all': i})

'''
def articles(request, page_number=1):
    all_articles = Article.objects.all()
    current_page = Paginator(all_articles, 2)
    return render_to_response('articles.html', {'articles': current_page.page(page_number)})
'''


def post_all(request, page_number=1):
    all_articles = Blog.objects.all().reverse().order_by('blog_date2')
    current_page = Paginator(all_articles, 3)
    return render_to_response('post_all.html', {'key_post_all': current_page.page(page_number),
                                                'username': auth.get_user(request).username})


'''
def post_one(request, pk=1):
    return render_to_response('post_one.html', {'key_post_one': Blog.objects.get(pk=pk),
                                                'key_comments': Comments.objects.filter(comments_articles_id=pk)})
'''


def post_one(request, pk=1):
    comment_form = CommentForm
    args = {}
    args.update(csrf(request))
    args['key_post_one'] = Blog.objects.get(pk=pk)
    args['key_comments'] = Comments.objects.filter(comments_articles_id=pk)
    args['form'] = comment_form
    args['username'] = auth.get_user(request).username
    return render_to_response('post_one.html', args)


def addlike(request, pk):
    try:
        if pk in request.COOKIES:
            redirect('/blog/details/%s/' % pk)
        else:
            addlike = Blog.objects.get(pk=pk)
            addlike.blog_likes += 1
            addlike.save()
            response = redirect('/blog/details/%s/' % pk)
            response.set_cookie(pk, 'test')
            return response
            # return redirect('/blog/details/%s/' % pk)
    except ObjectDoesNotExist:
        raise Http404
    return redirect('/blog/details/%s/' % pk)


def addcomment(request, pk):
    if request.POST:
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.comments_articles = Blog.objects.get(pk=pk)
            form.save()
    return redirect('/blog/details/%s/' % pk)
