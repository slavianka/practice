# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns


urlpatterns = [

]

urlpatterns += patterns('blog.views',
                        # Examples:
                        # url(r'^$', 'demofield.views.home', name='home'),
                        # url(r'^blog/', include('blog.urls')),
                        url(r'^$', 'post_all', name='post'),
                        url(r'^details/(?P<pk>[0-9]+)/$', 'post_one', name='post_one'),
                        url(r'^addlike/(?P<pk>[0-9]+)/$', 'addlike', name='addlike'),
                        url(r'^addcomments/(?P<pk>[0-9]+)/$', 'addcomment', name='addcomments'),
                        url(r'^page/(\d+)/$', 'post_all'),



                        )


