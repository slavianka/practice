# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField # импорт ckeditor


class Blog(models.Model):
    class Meta():
        db_table = 'blog'
        verbose_name_plural = 'блоги'
        verbose_name = 'блог'

    blog_title = models.CharField(max_length=150, verbose_name='название')
    # blog_text = models.TextField(blank=True, null=True, verbose_name='текст')
    blog_text = RichTextField(blank=True, null=True, verbose_name='текст') # указание creditor в самой модели
    blog_date = models.DateField(default=timezone.now, verbose_name='дата',)
    blog_date2 = models.DateTimeField(default=timezone.now, verbose_name='дата',)
    blog_likes = models.IntegerField(default=0, verbose_name='понравилось')
    blog_author = models.CharField(default='Вячеслав Кардаш', max_length=50, verbose_name='автор')

    def __unicode__(self):
        return self.blog_title


class Comments(models.Model):
    class Meta():
        db_table = 'comments'
        verbose_name_plural = 'комментарии'
        verbose_name = 'комментарий'

    comments_text = models.TextField(verbose_name='Текст комментария', max_length=5000)
    comments_articles = models.ForeignKey(Blog)


# master test 1