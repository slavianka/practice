# -*- coding: utf-8 -*-
from django.contrib import admin
from blog.models import Blog, Comments


class ArticleInline(admin.StackedInline):
    model = Comments
    extra = 1


class BlogAdmin(admin.ModelAdmin):
    fields = ['blog_title', 'blog_text', 'blog_date', 'blog_likes', 'blog_author', 'blog_date2']
    inlines = [ArticleInline]
    list_filter = ['blog_date']


admin.site.register(Blog, BlogAdmin)


