# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20160309_1456'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comments',
            name='comments_articles',
        ),
        migrations.DeleteModel(
            name='Comments',
        ),
    ]
