# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('blog_title', models.CharField(max_length=150, verbose_name=b'\xd0\xbd\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('blog_text', models.TextField(null=True, verbose_name=b'\xd1\x82\xd0\xb5\xd0\xba\xd1\x81\xd1\x82', blank=True)),
                ('blog_date', models.DateField(default=django.utils.timezone.now, verbose_name=b'\xd0\xb4\xd0\xb0\xd1\x82\xd0\xb0')),
                ('blog_likes', models.IntegerField(default=0, verbose_name=b'\xd0\xbf\xd0\xbe\xd0\xbd\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd0\xbb\xd0\xbe\xd1\x81\xd1\x8c')),
                ('blog_author', models.CharField(max_length=50, verbose_name=b'\xd0\xb0\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80')),
            ],
            options={
                'db_table': 'blog',
                'verbose_name_plural': '\u0431\u043b\u043e\u0433',
            },
        ),
    ]
