# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comments_text', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='blog',
            name='blog_author',
            field=models.CharField(default=b'Viacheslav Kardash', max_length=50, verbose_name=b'\xd0\xb0\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80'),
        ),
        migrations.AddField(
            model_name='comments',
            name='comments_articles',
            field=models.ForeignKey(to='blog.Blog'),
        ),
    ]
