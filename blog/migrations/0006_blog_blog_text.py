# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_remove_blog_blog_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='blog_text',
            field=ckeditor.fields.RichTextField(null=True, verbose_name=b'\xd1\x82\xd0\xb5\xd0\xba\xd1\x81\xd1\x82', blank=True),
        ),
    ]
