# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20160312_2035'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blog',
            options={'verbose_name': '\u0431\u043b\u043e\u0433', 'verbose_name_plural': '\u0431\u043b\u043e\u0433\u0438'},
        ),
        migrations.AlterModelOptions(
            name='comments',
            options={'verbose_name': '\u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', 'verbose_name_plural': '\u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0438'},
        ),
        migrations.AlterField(
            model_name='blog',
            name='blog_author',
            field=models.CharField(default=b'\xd0\x92\xd1\x8f\xd1\x87\xd0\xb5\xd1\x81\xd0\xbb\xd0\xb0\xd0\xb2 \xd0\x9a\xd0\xb0\xd1\x80\xd0\xb4\xd0\xb0\xd1\x88', max_length=50, verbose_name=b'\xd0\xb0\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80'),
        ),
        migrations.AlterField(
            model_name='comments',
            name='comments_text',
            field=models.TextField(max_length=5000, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82 \xd0\xba\xd0\xbe\xd0\xbc\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0\xd1\x80\xd0\xb8\xd1\x8f'),
        ),
    ]
