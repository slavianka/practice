# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_blog_blog_text'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blog',
            options={'ordering': ['blog_date'], 'verbose_name': '\u0431\u043b\u043e\u0433', 'verbose_name_plural': '\u0431\u043b\u043e\u0433\u0438'},
        ),
        migrations.AddField(
            model_name='blog',
            name='blog_date2',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'\xd0\xb4\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
        migrations.AlterField(
            model_name='comments',
            name='comments_text',
            field=models.TextField(verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82 \xd0\xba\xd0\xbe\xd0\xbc\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0\xd1\x80\xd0\xb8\xd1\x8f'),
        ),
    ]
