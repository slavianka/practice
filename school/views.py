from django.shortcuts import render, render_to_response


def school_main(request):
    return render_to_response('school.html')

